import React, { Component } from 'react'
import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    Dimensions,
    Image,
}from 'react-native';
import Icon from 'react-native-vector-icons/Fontisto';
// import { createIconSet } from 'react-native-vector-icons';
// const Icon = createIconSet('Fontisto');
export default class Home extends Component {
    render() {
        return (
            <View style = {{flex: 1}}>
                {/* TOP */}
                <View style = {StyleHome.home_top}>
                    <View style = {StyleHome.avatar}>
                        <Image style = {{width: 1*screenHeight/15, height: 1*screenHeight/15, borderRadius: 30}}
                        source = {require('../Image/avatar.jpg')}></Image>
                    </View>
                    <View style = {{marginTop: 20,marginLeft: 10,width: 2*screenWith/3 }}>
                        <Text>Mẫn</Text>
                        <Text>Thành viên mới</Text>
                    </View>
                    <Icon name = 'bell-alt' size = {20} style = {{marginTop: 20}}/>                    
                </View>
                {/* MID*/}
                <View style = {StyleHome.home_mid}>
                    <ScrollView>
                    <View style = {StyleHome.select}>
                        <View style = {StyleHome.detail}>
                            <View style = {StyleHome.over}>
                                <Icon name = 'qrcode' size = {25} color = "white"></Icon>
                            </View>
                            <Text>Tích điểm</Text>
                        </View>
                        <View style ={StyleHome.detail}>
                            <View style = {StyleHome.over}>
                                <Icon name = 'motorcycle' size = {25} color = "white"></Icon>
                            </View>
                            <Text>Đặt hàng</Text>
                        </View>
                        <View style = {StyleHome.detail}>
                            <View style = {StyleHome.over}>
                                <Icon name = 'shopping-package' size = {25} color = "white"></Icon>
                            </View>
                            <Text>Coupon</Text>
                        </View>
                        
                    </View>
                    <View>
                        <Text style = {StyleHome.text}>Dành cho bạn</Text>
                        <ScrollView horizontal = {true}>
                            <View style = {StyleHome.for_you}>
                                <View style = {StyleHome.images}>
                                    <Image style = {{width: 2*screenWith/3,height:2*screenWith/5,borderTopLeftRadius: 20,borderTopRightRadius:20}}
                                    source = {require('../Image/sim10so.jpg')}></Image>  
                                </View>
                                <View style = {StyleHome.header}>
                                    <Text style = {{fontSize:20,fontWeight: "normal",marginLeft: 15}}>Thông báo về chính sách thay đổi số điện thoại 11 số thành 10 số.</Text>
                                </View>
                            </View>
                            <View style = {StyleHome.for_you}>
                                <View style = {StyleHome.images}>
                                    <Image style = {{width: 2*screenWith/3,height:2*screenWith/5,borderTopLeftRadius: 20,borderTopRightRadius:20}}
                                        source = {require('../Image/taiapp.jpg')}></Image>
                                    
                                </View>
                                <View style = {StyleHome.header}>
                                    <Text style = {{fontSize:20,fontWeight: "normal",marginLeft: 15}}>Tích sao đã nhanh nay lại còn nhanh hơn với hệ thống với nhiều "Nhiệm vụ" hấp dẫn.</Text>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                    <View>
                        <ScrollView>
                        <Text style = {StyleHome.text}>Tin tức</Text>
                            <View style = {StyleHome.new}>
                                <View style = {StyleHome.images}>
                                    <Image style ={{width: screenWith - 40,height: 3*(screenWith - 40)/5, borderTopLeftRadius:20,borderTopRightRadius: 20}}
                                        source= {require('../Image/mua1tang1.jpg')}></Image>
                                   
                                </View>
                                <View style = {StyleHome.header}>
                                    <Text style = {{fontSize:20,fontWeight: "normal",marginLeft: 15}}>"Bật mí" 3 món quà xinh xắn tại The Coffee House đang áp dụng MUA 1 TẶNG 1 nhân dịp sinh nhật.</Text>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                    </ScrollView>
                </View>
                {/* BOT */}
                <View style = {StyleHome.home_bot}>
                    <View style = {StyleHome.icon}>
                        <Icon name = 'prescription' size = {30} color = '#FF8C00'></Icon>
                        <Text>Tin tức</Text>
                    </View>
                    <View style = {StyleHome.icon}>
                        <Icon name = 'motorcycle' size = {30} color = '#C0C0C0'></Icon>
                        <Text>Đặt hàng</Text>
                    </View>
                    <View style = {StyleHome.icon}>
                        <Icon name = 'nav-icon-a' size = {30} color = '#C0C0C0'></Icon>
                        <Text>Nhiệm vụ</Text>
                    </View>
                    <View style = {StyleHome.icon}>
                        <Icon name = 'home' size = {30} color = '#C0C0C0'></Icon>
                        <Text>Cửa hàng</Text>
                    </View>
                    <View style = {StyleHome.icon}>
                        <Icon name = 'person' size = {30} color = '#C0C0C0'></Icon>
                        <Text>Tài khoản</Text>
                    </View>
                </View>
            </View>
        )
    }
}
const screenWith = Dimensions.get('screen').width;
const screenHeight = Dimensions.get('screen').height;
const StyleHome = StyleSheet.create({
    home_top:{
        backgroundColor: 'white',
        flex: 1,
        flexDirection: 'row'
    },
    home_mid:{
        backgroundColor: '#E0E0E0',
        flex: 8,
    },
    home_bot:{
        backgroundColor: 'white',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems:'center',
    },
    select:{
        backgroundColor: '#E0E0E0',
        height: screenWith/3,
        flexDirection: 'row',
    },
    detail:{
        backgroundColor: 'white',
       width: (screenWith-80)/3,
       height: (screenWith-80)/3,
       justifyContent: 'center',
       alignItems:'center',
        marginTop: 10,
        marginLeft: 20,        
        borderRadius: 15
        
    },
    for_you:{
        backgroundColor: 'blue',
        width: 2*screenWith/3,
        height: 2*screenWith/3,
        marginLeft: 20,
        marginTop: 10,
        borderRadius: 20
    },
    text:{
        fontSize: 20,
        marginLeft: 20,
        fontWeight: "normal"
    },
    avatar:{
        backgroundColor: 'aqua',
        width: 1*screenHeight/15,
        height: 1*screenHeight/15,
        marginLeft: 20,
        marginTop: 10,
        borderRadius: 30
    },
    new:{
        backgroundColor: 'green',
        width: screenWith - 40,
        height: screenWith - 40,
        marginLeft: 20,
        marginTop: 10,
        borderRadius: 20
    },
    over: {
        backgroundColor: '#FF8C00',
        width: (screenWith-80)/7,
        height: (screenWith-80)/7,
        justifyContent: 'center',
        alignItems:'center',
        borderRadius: 30,
        marginBottom: 10
    },
    icon:{
        width: 70,
        height: 70,
        justifyContent: 'center',
        alignItems:'center',
    },
    images:{
        flex: 3,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        justifyContent: 'center',
        alignItems:'center',
    },
    header:{
        flex: 2,
        backgroundColor: 'white',
        borderBottomStartRadius: 20,
        borderBottomEndRadius: 20,
    }
})